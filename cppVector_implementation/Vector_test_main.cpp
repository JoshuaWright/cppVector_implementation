#define BOOST_TEST_MODULE Vector_test

#include <iostream>
#include "Vector.hpp"
#include <vector>
#include <boost\test\auto_unit_test.hpp>
using namespace std;

BOOST_AUTO_TEST_CASE(ctor) {
	{
		Vector<int> v;
		BOOST_CHECK(v.size() == 0);
		BOOST_CHECK(v.empty() == true);
		BOOST_CHECK(v.capacity() == 0);
	}
	{
		Vector<int> v(5);
		BOOST_CHECK(v.size() == 5);
		BOOST_CHECK(v.empty() == false);
		BOOST_CHECK(v.capacity() == 5);
	}
	{
		Vector<int> v(10, 34);
		BOOST_CHECK(v.size() == 10);
		BOOST_CHECK(v.empty() == false);
		BOOST_CHECK(v.capacity() == 10);

		for (int i = 0; i < v.size(); ++i)
			BOOST_CHECK(v[i] == 34);
	}
	{
		Vector<int> v{ 1,2,3,4,5,6,7 };
		BOOST_CHECK(v.size() == 7);
		BOOST_CHECK(v.empty() == false);
		BOOST_CHECK(v.capacity() == 7);

		for (int i = 0; i < v.size(); ++i)
			BOOST_CHECK(v[i] == i + 1);
	}

}

BOOST_AUTO_TEST_CASE(push_back) {

	Vector<int> v;
	v.push_back(32);
	BOOST_CHECK(v.size() == 1);
	BOOST_CHECK(v.empty() == false);
	BOOST_CHECK(v.capacity() == 1);
	v.push_back(43);
	BOOST_CHECK(v.size() == 2);
	BOOST_CHECK(v.empty() == false);
	BOOST_CHECK(v.capacity() == 2);
	v.push_back(34);
	BOOST_CHECK(v.size() == 3);
	BOOST_CHECK(v.empty() == false);
	BOOST_CHECK(v.capacity() == 4);
}

BOOST_AUTO_TEST_CASE(iterators) {
	Vector<int> v;
	vector<int> stdv;

	for (int i = 0; i < 10; ++i) {
		v.push_back(i + 1);
		stdv.push_back(i + 1);
	}

	BOOST_CHECK(stdv.size() == v.size());

	auto stdx = stdv.begin();
	for (auto x : v) 
		BOOST_CHECK(x == *(stdx++));	

	auto std_rit = stdv.rbegin();
	for (auto rit = v.rbegin(); rit != v.rend(); ++rit) {
		BOOST_CHECK(*rit == *(std_rit++));
	}

	BOOST_CHECK(v.pop_back() == 10);
	BOOST_CHECK(v.size() == 9);
}