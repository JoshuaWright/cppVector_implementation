/*
Author:	Joshua wright joshuawright1197@gmail.com
Date:	2017-05-21
File:	Vector.hpp
*/

#pragma once

#include <iterator>
#include <exception>

template<typename T>
class Vector {
public:

	// TYPES
	using size_type					= std::size_t;
	using value_type				= T;
	using pointer					= value_type*;
	using const_pointer				= const value_type*;
	using reference					= value_type&;
	using const_reference			= const value_type&;
	using iterator					= pointer;
	using const_iterator			= const pointer;
	using reverse_iterator			= std::reverse_iterator<iterator>;
	using const_reverse_iterator	= std::reverse_iterator<const_iterator>;

private:

	// MEMBERS
	pointer begPtr;
	pointer endPtr;
	pointer capPtr;

public:

	// CONSTRUCTORS
	Vector() : begPtr(nullptr), endPtr(nullptr) , capPtr(nullptr) {}
	Vector(size_type const&);
	Vector(size_type const&,value_type const&);
	Vector(std::initializer_list<T> const&);

	// DESTRUCTOR
	~Vector() { clear(); }

	// ELEMENT ACCESS
	reference		operator [] (size_type const& idx) { return begPtr[idx]; }
	const_reference operator [] (size_type const& idx) const { return begPtr[idx]; }
	reference		at(size_type const);
	const_reference at(size_type const) const;
	reference		back() { return *(endPtr - 1); }
	const_reference	back() const { return *(endPtr - 1); }
	reference		front() { return *begPtr; }
	const_reference	front() const { return *begPtr; }
	pointer			data() { return begPtr; }
	const_pointer	data() const { return begPtr; }

	// CAPACITY
	size_type		size() const { return endPtr - begPtr; }
	bool			empty() const { return size() == 0; }
	size_type		capacity() const { return capPtr - begPtr; }

	// ITERATORS
	iterator				begin() { return iterator(begPtr); }
	iterator				end() { return iterator(endPtr); }
	const_iterator			begin()	const { return const_iterator(begPtr); }
	const_iterator			end() const { return const_iterator(endPtr); }
	const_iterator			cbegin() const { return const_iterator(begPtr); }
	const_iterator			cend() const { return const_iterator(endPtr); }
	reverse_iterator		rbegin() { return reverse_iterator(endPtr); }
	reverse_iterator		rend() { return reverse_iterator(begPtr); }
	const_reverse_iterator	rbegin() const { return const_reverse_iterator(endPtr); }
	const_reverse_iterator	rend() const { return const_reverse_iterator(begPtr); }
	const_reverse_iterator	crbegin() const { return const_reverse_iterator(endPtr); }
	const_reverse_iterator	crend() const { return const_reverse_iterator(begPtr); }

	// MODIFIERS
	void		clear() { delete [] begPtr; }
	void		push_back(value_type const&);
	value_type  pop_back();

}; // end class Vector

template<typename T>
Vector<T>::Vector(size_type const& size) {
	begPtr = new value_type[size];
	capPtr = endPtr = begPtr + size;
}

template<typename T>
Vector<T>::Vector(size_type const& size,value_type const& value) {
	begPtr = new value_type[size];
	capPtr = endPtr = begPtr + size;
	for (size_type i = 0; i < size; ++i) {
		begPtr[i] = value;
	}
}

template<typename T>
Vector<T>::Vector(std::initializer_list<T> const& init) {
	endPtr = begPtr = new value_type[init.size()];
	for (auto e : init)
		*(endPtr++) = e;
	capPtr = endPtr;
}

template<typename T>
void Vector<T>::push_back(value_type const& value) {
	if (endPtr == capPtr) {
		int newCap = 0;
		if (size() != 0) {
			newCap = (capacity() * 2);
		}
		else {
			newCap = 1;
		}

		pointer newBeg = new value_type[newCap];
		capPtr = newBeg + newCap;

		for (size_type i = 0; i < size(); ++i) {
			newBeg[i] = begPtr[i];
		}
		endPtr = newBeg + size();
		delete[] begPtr;
		begPtr = newBeg;
	}

	begPtr[size()] = value;
	++endPtr;
}

template<typename T>
typename Vector<T>::value_type Vector<T>::pop_back() {
	value_type result = begPtr[size() - 1];
	begPtr[size() - 1] = NULL;
	--endPtr;
	return result;
}

template<typename T>
typename Vector<T>::reference Vector<T>::at(size_type const idx) {
	if (idx >= size())
		throw std::out_of_range("index out of range");
	return begPtr[idx];
}

template<typename T>
typename Vector<T>::const_reference Vector<T>::at(size_type const idx) const {
	if (idx >= size())
		throw std::out_of_range("index out of range");
	return begPtr[idx];
}